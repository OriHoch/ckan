# The Public Knowledge Workshop CKAN

This is a fork of [CKAN](http://ckan.org/) with updated translations (Hebrew / Arabic), fixes for RTL languages and other improvements.

See the [CKAN Documentation](http://docs.ckan.org) for installation and usage instructions.

The `master` branch is started from a stable tagged CKAN version, and changes are applied to it.

See the commits log for details of applied changes and the CKAN version it was based on.

For stable deployment, use the latest published hasadna-ckan release from the [Releases](https://github.com/hasadna/ckan/releases).

Translations are managed in [transifex](https://www.transifex.com/the-public-knowledge-workshop/hasadna-ckan/).

Push updated translations using the [release.sh](release.sh) script
